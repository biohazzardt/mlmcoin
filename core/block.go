package core

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"strings"
)

type BlockBody struct {
	transactions []*Transaction
}

type BlockHeader struct {
	id           int64
	datetime     string
	previousHash []byte
	merkleRoot   []byte
	hash         []byte
	nonce        int64
	bits         int64
	transCount   int
	size         uint64
}

type Block struct {
	header *BlockHeader
	body   *BlockBody
}

func (self *Block) ValidateTransactionsCount() bool {
	var res = true
	// var i = 0
	if len(self.header.merkleRoot) != len(self.body.transactions) {
		return false
	}
	for _, hash := range self.header.merkleRoot {
		var transactionFound = false
		for _, transaction := range self.body.transactions {
			if string(hash) == string(transaction.CalculateHash()) {
				transactionFound = true
			}
		}
		res = res && transactionFound
	}
	return res
}

func (self *Block) FilterTransactions() {
	var newTransactionList []*Transaction
	for _, t := range self.body.transactions {
		for _, h := range self.header.hash {
			if string(t.CalculateHash()) == string(h) {
				newTransactionList[len(newTransactionList)] = t
			}
		}
	}
	self.body.transactions = newTransactionList
	self.header.transCount = len(newTransactionList)
}

func (self *Block) Valid() bool {
	var bhash = self.CalculateHash()
	if hex.EncodeToString(self.header.hash) != hex.EncodeToString(bhash) {
		return false
	}
	if uint64(self.header.bits) > binary.BigEndian.Uint64(self.header.hash) {
		return false
	}
	return true
}

func (self *Block) CalculateHash() []byte {
	var selfHead = self.header
	var hasher = sha256.New()
	var data = strings.Join([]string{string(selfHead.previousHash), string(selfHead.nonce), string(selfHead.datetime), string(selfHead.bits)}, ":")
	hasher.Write([]byte(data))
	return hasher.Sum(nil)
}
