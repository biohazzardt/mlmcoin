package core

import (
	"math"
)

// Заголовок цепочки
type ChainHeader struct {
	id               int64  // ID блокчейна
	hash             []byte // Хэш
	name             string // Название
	initialReward    int    // Начальная награда за добытый блок
	downRewardBlocks int64  // Через сколько блоков уменьшается награда
}

// Цепочка
type Chain struct {
	header *ChainHeader   // Заголовок
	blocks map[int]*Block // Список блоков в цепочке
}

func (self *Chain) Valid() (bool, *Block, int64) {
	var currentBlockCount = len(self.blocks)
	if currentBlockCount == 0 {
		return false, nil, 0
	}
	var initialReward = 50.0
	var rewardDown = 250000.0
	var checkedWalletTransactions map[string]bool
	var rewardDownCount = math.Ceil(float64(currentBlockCount) / rewardDown)
	var currentReward float32
	if rewardDownCount == 0 {
		currentReward = float32(initialReward)
	} else {
		currentReward = float32(initialReward / (2.0 * rewardDownCount))
	}
	var c_id = currentBlockCount - 1
	var i int
	var b *Block
	var has, isset bool
	for i = c_id; i >= 0; i-- {
		b = self.blocks[i]
		for j, t := range b.body.transactions {
			if !t.CheckInChain(self, j == 0, int64(i)) {
				return false, self.blocks[i], int64(i)
			}
			has, isset = checkedWalletTransactions[string(t.walletID)]
			if !(isset && has) {

			}
		}
	}
}
