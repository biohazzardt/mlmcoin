package core

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/binary"
	"math"
	"math/big"
	"strings"
)

type TransactionIn struct {
	blockID       int64
	transactionID int
	out_id        int
}

type TransactionOut struct {
	coins     float64
	recipient []byte
}

type Transaction struct {
	id             []byte
	walletID       []byte
	currency       []byte
	transactIN     []*TransactionIn
	timestamp      []byte
	TransactionOut []*TransactionOut
	r, s           *big.Int
	key            *ecdsa.PublicKey
}

func (self *Transaction) CheckHash() bool {
	var hasher = sha256.New()
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, self.transactIN)
	var in_buf = buf.Bytes()
	buf.Reset()
	binary.Write(&buf, binary.BigEndian, self.TransactionOut)
	var out_buf = buf.Bytes()
	buf.Reset()
	var str = []string{string(self.currency), string(self.timestamp), string(in_buf), string(out_buf), string(self.walletID)}
	var data = strings.Join(str, ":")
	hasher.Write([]byte(data))
	var hash = hasher.Sum(nil)
	return ecdsa.Verify(self.key, hash, self.r, self.s)
}

func (self *Transaction) CalculateHash() []byte {
	var hasher = sha256.New()
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, self.transactIN)
	var in_buf = buf.Bytes()
	buf.Reset()
	binary.Write(&buf, binary.BigEndian, self.TransactionOut)
	var out_buf = buf.Bytes()
	buf.Reset()
	var data = strings.Join([]string{string(self.currency), string(self.timestamp), string(in_buf), string(out_buf), string(self.walletID)}, ":")
	hasher.Write([]byte(data))
	return hasher.Sum(nil)
}

func (self *Transaction) CheckInChain(c *Chain, is_reward bool, block int64) bool {
	var used_coins float64
	var target_coins float64
	if self.transactIN == nil {
		if self.currency != nil {
			if !self.CheckHash() {
				return false
			} else if self.key != c.blocks[0].body.transactions[0].key {
				return false
			} else {
				return true
			}
		} else {
			if block == 0 {
				return true
			} else {
				return false
			}
		}
	}
	if !self.CheckHash() {
		return false
	}
	if is_reward {
		var currentBlockCount = len(c.blocks)
		if currentBlockCount == 0 {
			return false
		}
		var initialReward = 50.0
		var rewardDown = 250000.0
		var rewardDownCount = math.Ceil(float64(currentBlockCount) / rewardDown)
		var currentReward float64
		if rewardDownCount == 0 {
			currentReward = initialReward
		} else {
			currentReward = initialReward / (2.0 * rewardDownCount)
		}
		if self.TransactionOut[0].coins != currentReward {
			return false
		}
	} else {
		used_coins = 0
		target_coins = 0
		for _, t_in := range self.transactIN {
			var b, bSet = c.blocks[int(t_in.blockID)]
			if !bSet {
				return false
			}
			if len(b.body.transactions) < t_in.transactionID {
				return false
			}
			if !b.body.transactions[t_in.transactionID].CheckInChain(c, false, t_in.blockID) {
				return false
			}
			if len(b.body.transactions[t_in.transactionID].TransactionOut) < t_in.out_id || string(b.body.transactions[t_in.transactionID].currency) != string(self.currency) {
				return false
			}
			var t_o = b.body.transactions[t_in.transactionID].TransactionOut[t_in.out_id]
			if string(t_o.recipient) != string(self.walletID) {
				return false
			}
			used_coins += t_o.coins
		}
		for _, t_out := range self.TransactionOut {
			target_coins += t_out.coins
		}
		if used_coins != target_coins {
			return false
		}
	}
	return true
}
