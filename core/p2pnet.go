package core

import (
	"crypto/sha256"
	"strings"
)

type P2PStatus int

const (
	//---------------------//
	P2P_OK P2PStatus = iota
	P2P_FAIL
	//---------------------//
	P2P_BLOCK_NEW
	P2P_BLOCK_CONFIRM
	P2P_BLOCK_CHECK
	P2P_BLOCK_FAILED
	P2P_SYNC_BLOCKS
	//---------------------//
	P2P_TRANSACTION_NEW
	P2P_TRANSACTION_CHECK_OK
	P2P_TRANSACTION_FAILED
	P2P_TRANSACTION_VALID
	P2P_TRANSACTION_IN_STACK
	P2P_TRANSACTION_CHECK
	//---------------------//
	P2P_CHECK_PEER
	P2P_CHECK_NETWORK
	P2P_UPDATE
	P2P_IMFINE
	P2P_SHUTDOWN
	P2P_GET_ADDR
)

type P2PPacket struct {
	senderID []byte
	body     string
	key      []byte
	hash     []byte
	sum      []byte
	status   P2PStatus
}

type P2PEventListener interface {
	onRecievePacket(packet *P2PPacket)
	onSendPacket(packet *P2PPacket)
}

type P2PClient struct {
	clientID  []byte
	privKey   []byte
	listeners []P2PEventListener
}

func (self *P2PClient) packetRecievedEvt(packet *P2PPacket) {
	//////CODE////////
	if string(packet.sum) != string(packet.calcHash()) {
		var p = new(P2PPacket)
		p.senderID = self.clientID
		p.body = "Data transmission error"
		p.status = P2P_FAIL
		self.sendPacket(p)
	}
	////Drop event////
	for _, e := range self.listeners {
		e.onRecievePacket(packet)
	}
}

func (self *P2PClient) sendPacket(packet *P2PPacket) {
	//////CODE////////

	////Drop event////
	for _, e := range self.listeners {
		e.onSendPacket(packet)
	}
}

func (self *P2PPacket) calcHash() []byte {
	var hasher = sha256.New()
	var str = strings.Join([]string{string(self.senderID), string(self.body), string(self.key), string(self.hash), string(self.status)}, ":")
	hasher.Write([]byte(str))
	return hasher.Sum(nil)
}

func p2phandlerFunc() {

}
