package main

import (
	"crypto/sha256"
	"fmt"
	"math"
	"math/rand"
)

type OpenKey struct {
	p int
	g int
	y int
}

type KeyGenResult struct {
	openKey   OpenKey
	secretKey int
}

type CryptMessage struct {
	openKey OpenKey
	message string
	hash    []byte
}

// Testings

func main() {
	var msg *CryptMessage = new(CryptMessage)
	var key OpenKey
	var kgr = GenerateKeys()
	key = kgr.openKey
	var secret = kgr.secretKey
	fmt.Printf("El-Gamal digital cer:\n")
	fmt.Printf("Keys generated, your public key: (%v, %v, %v)\n", key.p, key.g, key.y)
	fmt.Printf("Your secret key %v\n", secret)
	var message string
	fmt.Scanf("%s", &message)
	msg.message = message
}

func GenerateKeys() *KeyGenResult {
	var rez *KeyGenResult = new(KeyGenResult)
	var p = rand.Intn(9999999-100) + 100
	rez.openKey.p = p
	var g = rand.Intn(p-98) + 96
	rez.openKey.g = g
	var x = rand.Intn(p-2) + 1
	rez.secretKey = x
	var y = math.Mod(float64(math.Pow(float64(g), float64(x))), float64(p))
	rez.openKey.y = int(y)
	return rez
}

func EncryptMessage(message string, key int) *CryptMessage {
	var msg *CryptMessage = new(CryptMessage)
	var M = sha256.Sum256([]byte(message))
	msg.message = message

	return msg
}
